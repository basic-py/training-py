from openpyxl import Workbook

def main():
    workbook = Workbook()
    sheet = workbook.active

    sheet["A1"] = "hello"
    sheet["B1"] = "world!"

    workbook.save(filename="hello_world.xlsx")

if __name__ == "__main__":
    main()