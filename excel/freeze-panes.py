from openpyxl import load_workbook
from openpyxl.formatting.rule import IconSetRule

workbook = load_workbook(filename="D:/training-py/excel/hello_world.xlsx")
sheet = workbook.active
# sheet.freeze_panes = "B1"
# workbook.save("D:/training-py/excel/sample_frozen.xlsx")

# Check the used spreadsheet space using the attribute "dimensions"
# print(sheet.dimensions)
# sheet.auto_filter.ref = "D2:G2"
# workbook.save(filename="D:/training-py/excel/sample_filter.xlsx")

#
icon_set_rule = IconSetRule("5Arrows", "num", [1, 2, 3, 4, 5])
sheet.conditional_formatting.add("H2:H10", icon_set_rule)
workbook.save("D:/training-py/excel/sample_conditional_formatting_icon_set.xlsx")