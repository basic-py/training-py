import json

# read json file

with open("D:/training-py/test.json") as val:
    json_data = json.load(val)
    print(json.dumps(json_data, indent=4))


# write json file
'''
data = {}
data['people'] = []
data['people'].append({
    'name': 'Scott',
    'website': 'stackabuse.com',
    'from': 'Nebraska'
})
data['people'].append({
    'name': 'Larry',
    'website': 'google.com',
    'from': 'Michigan'
})
data['people'].append({
    'name': 'Tim',
    'website': 'apple.com',
    'from': 'Alabama'
})

with open("D:/training-py/data.json", "w") as outfile:
    json.dump(data, outfile)
'''