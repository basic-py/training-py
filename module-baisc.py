import time
import functools

#T time module example
seconds = time.time()
print("Seconds since epoch =", seconds)

# seconds passed since epoch
seconds = 1545925769.9618232
local_time = time.ctime(seconds)
print("Local time:", local_time)

named_tuple = time.localtime() # get struct_time
time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
print(time_string)

#Lambda funtion example
# syntax
'''
    lambda x : x
    x = lambda x,y: x +y
    x(2,4)
'''
test = lambda x : x
print(test("print x"))

high_ord_func = lambda x, func: x + func(x)
print(high_ord_func(2, lambda x: x*x))

oddOrEven = lambda x: x%2 and 'odd' or 'even'
print(oddOrEven(3))
# map, filter, reduce

listBeforeMap = [1,2,3,4,5,6]
listAfterMap = list(map(lambda x: x + 1, listBeforeMap))
print(listAfterMap)

listAfterFilter = list(filter(lambda x: x > 2, listBeforeMap))
print(listAfterFilter)

listAfterReduce = lambda x,y : x if x < y else y # find min in list
reduceTest = functools.reduce(listAfterReduce,listBeforeMap)
print(reduceTest)




