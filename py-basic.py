# Statements
x = 0
y = 5
if x < y:
    print('yes')
# Variables
# The Input Function
def testMethod(test):
    print("hello " + test)

testMethod("world!")
# Strings Ex: a = "1"
# Numbers EX: a = 1
# Lists and Tuples
# List
a = ['foo', 'bar', 'baz', 'qux', 'quux', 'corge']
print(a)
# 1.Lists Are Ordered
b = ['baz', 'qux', 'bar', 'foo']
a == b
# 2.Lists Can Contain Arbitrary Objects
e = [21.42, 'foobar', 3, 4, 'bark', False, 3.14159]
print(e)
# 2.1. The expression a[m:n] a[2:4] => ['baz', 'qux']
# 2.2. You can specify a stride—either positive or negative a[0:6:2] => ['foo', 'baz', 'quux']
# 2.3. Reversing a list a[::-1] => ['corge', 'quux', 'qux', 'baz', 'bar', 'foo']
# Tuple
t = ('foo', 'bar', 'baz', 'qux')
(s1, s2, s3, s4) = t #assign in tuple
print(t)
print(s1)
print(s2)
# Dictionaries
'''
    d = {
        <key>: <value>,
        <key>: <value>,
        .
        .
        .
        <key>: <value>
    }

    1) list_tuple = dict([
         ('Colorado'    , 'Rockies'),
         ('Boston'      , 'Red Sox'),
         ('Minnesota'   , 'Twins'),
         ('Milwaukee'   , 'Brewers'),
         ('Seattle'     , 'Mariners')
    ]);

    2) MLB_team = dict (
        Colorado    =   'Rockies',
        Boston      =   'Red Sox',
        Minnesota   =   'Twins',
        Milwaukee   =   'Brewers',
        Seattle     =   'Mariners'
        )
'''
# Booleans Ex: flag = True/False
# Conditionals (if, elif, else)
x = 1
y = 2
if x == y:
    print( " x = y")
elif x > y:
    print( " x > y")
else:
    print( " x != y")

# AND / OR operators
# While Loops
n = 5
while n > 0:
    n -= 1
    if n == 2:
        break
    print(n)
# For Loops

listTest = ['foo', 'bar', 'baz']
for i in listTest:
    print("ele: ", i)

for idx,val in enumerate(listTest):
    print(idx, val)


iter('foobar')                             # String
#>>> <str_iterator object at 0x036E2750>

iter(['foo', 'bar', 'baz'])                # List
#>>><list_iterator object at 0x036E27D0>

iter(('foo', 'bar', 'baz'))                # Tuple
#>>><tuple_iterator object at 0x036E27F0>

iter({'foo', 'bar', 'baz'})                # Set
#>>><set_iterator object at 0x036DEA08>

iter({'foo': 1, 'bar': 2, 'baz': 3})       # Dict
#>>><dict_keyiterator object at 0x036DD990>

a = ['foo', 'bar', 'baz']
itr = iter(a)
list(itr)
#>>>['foo', 'bar', 'baz']
next(itr) # 'foo'
next(itr) # 'bar'
next(itr) # 'baz'

# Data Validation
'''
data = 0
while (data <= 0):
    value = input('Enter positive integer value : ')
    if value.isdigit():     # returns true if all digits otherwise false
        data = int(value)   # casts value to integer
        break               # breaks out of while loops
print('Value squared=:',data*data)
'''
# Error Handling
'''
try:
    print(1/0)
except IOError:
    print('An error occurred trying to read the file.')
except ValueError:
    print('Non-numeric data found in the file.')
except ImportError:
    print ("NO module found")
except EOFError:
    print('Why did you do an EOF on me?')
except KeyboardInterrupt:
    print('You cancelled the operation.')
except:
    print('An error occurred.')
'''
# Functions
print(len([1,2,3,4,5,6]))
print(any([False, False, False]))
print(any(['bar' == 'baz', len('foo') == 3, 'qux' in {'foo', 'bar', 'baz'}]))