# -*- coding: utf-8 -*-

import requests
import feedparser
import webbrowser

# url = "https://vnexpress.net/rss/tin-moi-nhat.rss"

# feed = feedparser.parse(url)

# print(feed['feed']['link'])

# for post in feed.entries:
#     print(post.title + ": " + post.link[0] + " ")

# webbrowser.open(feed.entries[0].link)

# api-endpoint 
URL = "https://maps.googleapis.com/maps/api/geocode/json"
  
# location given here 
location = "delhi technological university"

# defining a params dict for the parameters to be sent to the API 
PARAMS = {'address':location, 'key':'GG API KEY'} 

# sending get request and saving the response as response object 
r = requests.get(url = URL, params = PARAMS) 

# extracting data in json format 
data = r.json()

print(data['results'])